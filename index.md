---
layout: home

hero:
  name: "pensieri sparsi"
  tagline: Un luogo dove sintetizzare il groviglio della mia mente

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

