import { defineConfig } from 'vitepress'

export default defineConfig({
  title: "pensierisparsi",
  description: "Un luogo dove sintetizzare il groviglio della mia mente",
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' }
    ],

    search: {
      provider: 'local'
    },

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/ErosPingani1/pensierisparsi-blog' }
    ]
  }
})
